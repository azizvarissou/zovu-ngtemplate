import { Component, OnInit } from '@angular/core';

import { ZXingScannerComponent } from '@zxing/ngx-scanner';

@Component({
  selector: 'zv-id-page',
  templateUrl: './id-page.component.html',
  styleUrls: ['./id-page.component.scss']
})
export class IdPageComponent implements OnInit {

  constructor(private a: ZXingScannerComponent) { }
  autostart = true;
  ngOnInit(): void {
    this.a.autostart = true;
    console.log(this.a);

  }
  scan(event) {
    window.open(event, '_blank');
  }

}
