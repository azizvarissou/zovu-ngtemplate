import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdPageComponent } from './id-page.component';

describe('IdPageComponent', () => {
  let component: IdPageComponent;
  let fixture: ComponentFixture<IdPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
