import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'zv-step-widget',
  templateUrl: './step-widget.component.html',
  styleUrls: ['./step-widget.component.scss']
})
export class StepWidgetComponent implements OnInit {

  constructor() { }

  @Input() icon;
  @Input() title;
  @Input() description;
  ngOnInit(): void {
  }

}
