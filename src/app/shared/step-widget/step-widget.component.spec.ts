import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepWidgetComponent } from './step-widget.component';

describe('StepWidgetComponent', () => {
  let component: StepWidgetComponent;
  let fixture: ComponentFixture<StepWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
