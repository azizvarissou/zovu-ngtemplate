import { CommonModule } from '@angular/common';
import { IconsModule } from '../icons/icons.module';
import { NgModule } from '@angular/core';
import { StepWidgetComponent } from './step-widget/step-widget.component';

@NgModule({
  declarations: [StepWidgetComponent],
  imports: [
    CommonModule,
    IconsModule
  ],
  exports: [StepWidgetComponent]
})
export class SharedModule { }
