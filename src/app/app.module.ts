import { ZXingScannerComponent, ZXingScannerModule } from '@zxing/ngx-scanner';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { ContactComponent } from './contact/contact.component';
import { HelpComponent } from './help/help.component';
import { IconsModule } from './icons/icons.module';
import { IdPageComponent } from './id-page/id-page.component';
import { NgModule } from '@angular/core';
import { SharedModule } from './shared/shared.module';
import { NavbarComponent } from './navbar/navbar.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    HelpComponent,
    ContactComponent,
    IdPageComponent,
    NavbarComponent,
    NotfoundComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    IconsModule,
    ZXingScannerModule
  ],
  providers: [ZXingScannerComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
