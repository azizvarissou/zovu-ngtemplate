import { RouterModule, Routes } from '@angular/router';

import { ContactComponent } from './contact/contact.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HelpComponent } from './help/help.component';
import { IdPageComponent } from './id-page/id-page.component';
import { NgModule } from '@angular/core';
import { NotfoundComponent } from './notfound/notfound.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/scan',
    pathMatch: 'full'
  },
  {
    path: 'DashBoard',
    component: DashboardComponent
  },
  {
    path: 'scan',
    component: IdPageComponent
  },
  {
    path: 'help',
    component: HelpComponent
  },

  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: '**',
    component: NotfoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
