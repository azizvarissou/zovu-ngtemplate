import { CheckCircle, FileText, HelpCircle, Home, LogIn, LogOut, Mail, Maximize, PhoneCall } from 'angular-feather/icons';

import { CommonModule } from '@angular/common';
import { FeatherModule } from 'angular-feather';
import { NgModule } from '@angular/core';

// Select some icons (use an object, not an array)
const icons = {
  LogIn,
  CheckCircle,
  FileText,
  PhoneCall, Mail, Home, HelpCircle, LogOut, Maximize
};



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FeatherModule.pick(icons)
  ],
  exports: [FeatherModule]
})
export class IconsModule { }
